$(document).ready(function () {
    new WOW().init(); //wow js
    $('.burger-nav').on('click', function () {
        $('.container nav ul').toggleClass('open');
    });

    $('.js-wp-2').waypoint(function (direction) {
        $('.js-wp-2').addClass('animated slideInUp');
    }, {
        offset: '70%'
    });

    $('.iphone-btn').delay(1500).animate({
            top: '3'
        }, //css
        150, //time
        function () { //callback function
            $('.js-wp-3').addClass('animated fadeIn');
            $('.iphone-btn').delay(300).animate({
                top: '0'
            }, 150);
        });
});